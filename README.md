# Aplicación Movil para prevención y auxilio de personas en situación VIF en la comuna de Quilicura

## Resumen

El presente repositorio contiene los archivos y scripts de la aplicación móvil para prevención y auxilio de personas que se encuentren en situación de violencia intrafamiliar en la comuna de Quilicura. 

La aplicación consiste en un sistema camuflado, con el fin de proteger la acción de pedir auxilio a través de la misma. En primer lugar, la aplicación realizará las siguientes acciones:

- Registro de Usuaria
- Registro de contacto.
- Ingreso a la aplicación.
- Información
- Configuración.
- Envío de alerta a contacto de emergencia.
- Envío de posicionamiento en tiempo real a contacto de emergencia.
- Alerta Telegram a Seguridad Pública.
- Mensaje de Peligro Inminente y posición en tiempo real a Seguridad Pública

## Requerimientos

- Node.js
- React Native
- Android Studio


