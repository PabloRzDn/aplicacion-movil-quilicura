import React, { useState } from "react";
import { View, Text, Button, ScrollView, TextInput, StyleSheet, Pressable, TouchableOpacity, Modal } from "react-native";
import { TouchableHighlight } from "react-native-gesture-handler";
import firebase from "../database/firebase";




const Separadorlargo = () => (
    <View style={estilos.separadorlargo} />
);

const Separadorcorto = () => (
    <View style={estilos.separadorcorto} />
);


const CrearContacto = (props) => {
    const [state, setState] = useState({
        nombres: "",
        apellidos: "",
        email: "",
        telefono: ""
    });





    const handleChangeText = (name, value) => {
        setState({ ...state, [name]: value })
    }

    const AddNewContact = async () => {
        console.log(state)
        await firebase.db.collection('contacts').add({
            nombres: state.nombres,
            apellidos: state.apellidos,
            email: state.email,
            telefono: state.telefono
        })
        alert("Contacto añadido exitosamente")
        props.navigation.navigate("Index");
    }




    return (
        <ScrollView style={estilos.Fondo}>

            <Separadorcorto />
            <View>
                <Text style={estilos.Titulos}>REGISTRO CONTACTOS</Text>
            </View>
            <Separadorlargo />
            <Separadorcorto />
            <Text style={estilos.TituloCampos}>Nombres Contacto</Text>
            <View style={estilos.Entradas}>
                <TextInput style={estilos.TextoPlaceholder}
                    placeholder="Nombre"
                    onChangeText={(value) => handleChangeText("nombres", value)} />
            </View>

            <Text style={estilos.TituloCampos}>Apellidos Contacto</Text>
            <View style={estilos.Entradas}>
                <TextInput style={estilos.TextoPlaceholder}
                    placeholder="Apellidos"
                    onChangeText={(value) => handleChangeText("apellidos", value)} />
            </View>

            <Text style={estilos.TituloCampos}>Teléfono</Text>
            <View style={estilos.Entradas}>
                <TextInput style={estilos.TextoPlaceholder}
                    placeholder="Teléfono"
                    onChangeText={(value) => handleChangeText("telefono", value)} />
            </View>

            <Text style={estilos.TituloCampos}>Email</Text>
            <View style={estilos.Entradas}>
                <TextInput style={estilos.TextoPlaceholder}
                    placeholder="Email"
                    onChangeText={(value) => handleChangeText("email", value)} />
            </View>



            <Separadorlargo />
            <Separadorlargo />

            <Button title="FINALIZAR" color="#069213" onPress={() => AddNewContact()} />





        </ScrollView>

    )
}


const estilos = StyleSheet.create({
    Fondo: {
        margin: 0,
        padding: 0,
        backgroundColor: '#4c2882'

    },
    Titulos: {
        textAlign: "center",
        color: "#EDFFEF",
        fontWeight: "bold",
        fontSize: 15

    },
    TituloCampos: {
        marginLeft: 25,
        color: "#EDFFEF",
        fontWeight: "bold",
        fontSize: 13

    },


    Entradas: {
        flex: 1,
        flexDirection: "row",
        flexWrap: 'wrap',
        padding: 0,
        borderRadius: 15,
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 15,
        marginRight: 15,
        height: 27,
        backgroundColor: "#FED8F5"

    },
    Boton: {
        alignItems: "stretch",
        justifyContent: "center",
        flex: 1,
        padding: 0,
        borderRadius: 15,
        margin: 0,
        backgroundColor: "#069213"

    },
    TextoBoton: {
        color: "#EDFFEF",
        fontWeight: "bold",
        fontSize: 20
    },
    TextoPlaceholder: {
        marginTop: 0,
        marginLeft: 20,
        fontSize: 16

    },
    separadorlargo: {
        marginVertical: 20,
    },
    separadorcorto: {
        marginVertical: 12.360,
    }

})


export default CrearContacto
