import React from "react";
import { Text,ScrollView,View, StyleSheet,TextInput, Button } from "react-native";
import { RecordarContrasena } from "../componentes/RecordarContrasena";

const Separador=()=>{
    return (
        <View style={estilos.Separador}/>

    )
}

const Separadorcorto=()=>{
    return(
        <View style={estilos.Separadorcorto}/>
    )
}

const Login=()=>{
    return (
        <ScrollView style={estilos.Contenedor}>
            <View>
                <TextInput style={estilos.Entradas}/>
            </View>
            <Separador/>
            <View>
                <Button title="INGRESAR" color="#069213"/>
            </View>
            <Separadorcorto/>
            <View style={estilos.TextoClick}>
                <RecordarContrasena/>
                
            </View>
        </ScrollView>



    )
}

const estilos=StyleSheet.create({
    Contenedor:{
        margin:0,
        padding:0,
        backgroundColor:'#B489F5'
    },
    Entradas:{
        flex:1,
        flexDirection:"row",
        flexWrap: 'wrap',
        padding :0,
        borderRadius:15,
        marginTop:"80%",
        marginBottom:10,
        marginLeft:100,
        marginRight:100,
        height:35,
        backgroundColor:"#fcfcfc",
        textAlign:"center"

    },
    Separador:{
        marginVertical: 20,
    },
    Separadorcorto:{
        marginVertical: 12.360,
    },
   

})

export default Login