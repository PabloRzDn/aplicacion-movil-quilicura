import React, { useState } from "react";
import { View, Text, Button, ScrollView, TextInput, StyleSheet, Pressable, Picker } from "react-native";
import firebase from "../database/firebase";


const Separadorlargo = () => (
    <View style={estilos.separadorlargo} />
);

const Separadorcorto = () => (
    <View style={estilos.separadorcorto} />
);

const CrearUsuario = (props) => {
    const [state, setState] = useState({
        nombres: "",
        apellidos: "",
        direccion: "",
        numero: "",
        departamento: "",
        email: "",
        telefono: "",
        contrasena: ""
    });

    const handleChangeText = (name, value) => {
        setState({ ...state, [name]: value })
    }


    const AddNewUser = async () => {
        console.log(state)
        await firebase.db.collection('users').add({
            nombres: state.nombres,
            apellidos: state.apellidos,
            direccion: state.direccion,
            numero: state.numero,
            departamento: state.departamento,
            email: state.email,
            telefono: state.telefono,
            contrasena: state.contrasena
        })
        alert("Datos de usuaria almacenados correctamente");
        props.navigation.navigate("AnadirContacto");
    }

    return (
        <ScrollView style={estilos.Fondo}>
            <Separadorcorto />
            <View>
                <Text style={estilos.Titulos}>REGISTRO USUARIA</Text>
            </View >

            <Separadorcorto />
            <Text style={estilos.TituloCampos}>Nombres</Text>
            <View style={estilos.Entradas}>
                <TextInput style={estilos.TextoPlaceholder}
                    placeholder="Nombres"
                    onChangeText={(value) => handleChangeText("nombres", value)} />
            </View>

            <Text style={estilos.TituloCampos}>Apellidos</Text>
            <View style={estilos.Entradas}>
                <TextInput style={estilos.TextoPlaceholder}
                    placeholder="Apellidos"
                    onChangeText={(value) => handleChangeText("apellidos", value)} />
            </View>
            <Text style={estilos.TituloCampos}>Calle o Avenida</Text>
            <View style={estilos.Entradas}>
                <TextInput style={estilos.TextoPlaceholder}
                    placeholder="Calle o Avenida"
                    onChangeText={(value) => handleChangeText("direccion", value)} />
            </View>
            <View style={estilos.ContenedorEntradaCorta}>
                <Text style={estilos.TituloCampos}>Número</Text>
                <Text style={estilos.TituloCampos}>Departamento</Text>
            </View>

            <View style={estilos.ContenedorEntradaCorta}>



                <View style={estilos.EntradasCortas}>
                    <TextInput style={estilos.TextoPlaceholder}
                        placeholder="Número"
                        onChangeText={(value) => handleChangeText("numero", value)} />
                </View>


                <View style={estilos.EntradasCortas}>
                    <TextInput style={estilos.TextoPlaceholder}
                        placeholder="Departamento"
                        onChangeText={(value) => handleChangeText("departamento", value)} />
                </View>


            </View>




            <Text style={estilos.TituloCampos}>Email</Text>
            <View style={estilos.Entradas}>
                <TextInput style={estilos.TextoPlaceholder}
                    placeholder="Email"
                    onChangeText={(value) => handleChangeText("email", value)} />
            </View>
            <Text style={estilos.TituloCampos}>Teléfono</Text>
            <View style={estilos.Entradas}>
                <TextInput style={estilos.TextoPlaceholder}
                    placeholder="Teléfono"
                    onChangeText={(value) => handleChangeText("telefono", value)} />
            </View>
            <View style={estilos.ContenedorEntradaCorta}>
                <Text style={estilos.TituloCampos}>Contraseña</Text>
                <Text style={estilos.TituloCampos}>Repetir Contraseña</Text>
            </View>

            <View style={estilos.ContenedorEntradaCorta}>


                <View style={estilos.EntradasCortas}>
                    <TextInput style={estilos.TextoPlaceholder}
                        placeholder="Contraseña"
                        onChangeText={(value) => handleChangeText("contrasena", value)} />
                </View>


                <View style={estilos.EntradasCortas}>
                    <TextInput style={estilos.TextoPlaceholder} placeholder="Contraseña" />
                </View>


            </View>

            <Separadorlargo />
            <View style={estilos.Boton}>
                <Button title="CONTINUAR" color="#069213" onPress={() => AddNewUser()} />


            </View>
        </ScrollView>
    )
}

const estilos = StyleSheet.create({
    Fondo: {
        margin: 0,
        padding: 0,
        backgroundColor: '#4c2882'
    },

    Titulos: {
        textAlign: "center",
        color: "#EDFFEF",
        fontWeight: "bold",
        fontSize: 15

    },
    TituloCampos: {
        marginLeft: 25,
        color: "#EDFFEF",
        fontWeight: "bold",
        fontSize: 13

    },
    Entradas: {
        flex: 1,
        flexDirection: "row",
        flexWrap: 'wrap',
        padding: 0,
        borderRadius: 15,
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 15,
        marginRight: 15,
        height: 27,
        backgroundColor: "#FED8F5"

    },

    ContenedorEntradaCorta: {

        flexDirection: "row",
        justifyContent: "space-around"
    },


    EntradasCortas: {
        flexBasis: "42%",
        padding: 0,
        borderRadius: 15,
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 15,
        marginRight: 15,
        height: 27,
        backgroundColor: "#FED8F5"

    },
    Boton: {
        alignItems: "stretch",
        justifyContent: "center",
        flex: 1,
        padding: 0,
        borderRadius: 15,
        margin: 0,
        backgroundColor: "#069213"

    },
    TextoBoton: {
        color: "#EDFFEF",
        fontWeight: "bold",
        fontSize: 20
    },
    TextoPlaceholder: {
        marginLeft: 20,
        fontSize: 16

    },
    separadorlargo: {
        marginVertical: 10,
    },
    separadorcorto: {
        marginVertical: 5,
    }


})


export default CrearUsuario