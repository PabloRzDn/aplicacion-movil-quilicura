import React, { Component,useEffect } from 'react';
import MapView from 'react-native-maps';
import {NavigationContainer} from '@react-navigation/native';
import { createStackNavigator, useNavigation  } from "@react-navigation/stack";
import { View, Text, Button, ScrollView, TextInput, StyleSheet, Pressable, TouchableOpacity, Modal, Dimensions } from "react-native";
import * as Location from "expo-location";
import { useState } from "react/cjs/react.development";

export default function Localizacion() {
    const [location,setLocation]=useState(null);
    const [errorMsg,setErrorMsg]=useState(null);
    const coord=null;
    const states={
        latitude:null,
        longitude:null,
    }

    useEffect(()=>{
        (async()=>{
            let {status}=await Location.requestForegroundPermissionsAsync();
            if(status!=="granted"){
                setErrorMsg("Los permisos de acceso a la ubicación han sido denegados");
                return;
            } 
            let location=await Location.getCurrentPositionAsync({});
            const {latitude,longitude}=location.coords;
            setLocation(location);
              })();
            },[]);
            let text="Esperando...";
            if (errorMsg){
                text=errorMsg;

            } else if (location){
                
                text=JSON.stringify(location);
            }
        return( <ScrollView>

            <Text>{text} </Text>
            

            <MapView style={styles.map} />

        </ScrollView>
             )

}


const styles=StyleSheet.create({
    map:{
        width:Dimensions.get('window').width,
        height:Dimensions.get("window").height,
    },
})


/*
const [location,setLocation]=useState(null);
    const [errorMsg,setErrorMsg]=useState(null);

    useEffect(()=>{
        (async()=>{
            let {status}=await Location.requestForegroundPermissionsAsync();
            if(status!=="granted"){
                setErrorMsg("Los permisos de acceso a la ubicación han sido denegados");
                return;
            } 
            let location=await Location.getCurrentPositionAsync({});
            setLocation(location);
              })();
            },[]);
            let text="Esperando...";
            if (errorMsg){
                text=errorMsg;

            } else if (location){
                text=JSON.stringify(location);
            }
*/
