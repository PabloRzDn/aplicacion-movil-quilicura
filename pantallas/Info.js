import React from "react";
import {Text,View,ScrollView, StyleSheet,SafeAreaView, Image,Button} from "react-native";

const Info=()=>{
    return(
            
                <View style={[estilos.Contenedor,{
                    flexDirection:"column"
                }]}>
                <View style={estilos.Descripcion}>
                <Text style={estilos.TextoDescripcion}>La aplicación que tienes en tus manos
                es una herramienta a través de la cual puedes revelar, en forma secreta y
                segura, si estás en alguna situación de riesgo, violencia de género o intrafamiliar.</Text>
                    </View> 

                    <View style={estilos.Informacion}>
                    
                    
                        <View style={estilos.Columna1}>
                            <View style={{...estilos.itemIconos,backgroundColor:"#31a86d"}}>
                                <Image style={estilos.Iconos} source={require("../statics/cookies-white.png")}/>
                            </View>
                            <View style={{...estilos.itemIconos,backgroundColor:"#a2a831"}}>
                                <Image style={estilos.Iconos} source={require("../statics/ram-white.png")}/>
                            </View>
                               
                            <View style={{...estilos.itemIconos,backgroundColor:"#A83131"}}>
                             <Image style={estilos.Iconos} source={require("../statics/memory-card-white.png")}/> 
                            </View>
                        </View>

                        <View style={estilos.Columna2}>
                        <View style={estilos.itemDescripciones}>
                        <Text style={estilos.Titulos}>Borrar Cookies</Text>
                                <Text style={estilos.TextoDescripcion}>Con este botón envia el mensaje
                                "llámame" a tu contacto de emergencia seguro.</Text>
                            </View>
                            <View style={estilos.itemDescripciones}>
                            <Text style={estilos.Titulos}>Optimizar RAM</Text>
                                <Text style={estilos.TextoDescripcion}>Comparte la ubicación actual y
                                envia el mensaje "Estoy aquí" a tu contacto de emergencia seguro.</Text>
                            </View>
                           
                            <View style={estilos.itemDescripciones}>
                            <Text style={estilos.Titulos}>Reiniciar Sistema</Text>
                                <Text style={estilos.TextoDescripcion}>Envía un mensaje de "estoy en peligro" a seguridad Pública.
                                    Comparte tu ubicación y asigna un QR con el lugar y la hora.
                                </Text>
                            </View>
                            
                        </View>
                        
                        
                        
                        
                    
                    
                    </View>  
                        <View style={estilos.Boton}>
                            <Button color="#069213" title="Volver"/>
                        </View> 
                    
                </View>
            
    
        
    )
}

const estilos=StyleSheet.create({
    
    Fondo:{
        flex:1,
        margin:0,
        padding:0,
        backgroundColor:"#4c2882",

    },
    Contenedor:{
        flex:1,
        padding:0,
        margin:0,
        backgroundColor:"#4c2882"
    },
    Descripcion:{
        flex:1,
        padding:5,
        backgroundColor:"#4c2882",
        flexDirection:"row",
        textAlign:"center"

    },
    Informacion:{
        flex:4,
        flexDirection:"row",
        backgroundColor:"#4c2882"
       
                
    },
    Columna1:{
        flex:1,
        width:"30%",
        backgroundColor:"blue",
        alignItems:"center",
        backgroundColor:"#4c2882"
        
    },
    itemIconos:{
        
        height:100,
        width:100,
        alignItems:"center",
        justifyContent:"center",
        borderRadius:10,
        padding:0,
        marginTop:10,
        marginBottom:10,
        marginLeft:10,
        marginRight:10
    },
    itemDescripciones:{
        marginTop:5,
        height:"30%",
        width:"100%",
        alignItems:"center",
        backgroundColor:"#4c2882"

    },
   

    Columna2:{
        flex:2,
        backgroundColor:"green",
        alignItems:"flex-start",
        backgroundColor:"#4c2882"
    },
    Titulos:{
        fontSize:16,
        fontWeight:"bold",
        color:"#f7edff",
        margin:0,
        textAlign:"left"
    },

    TextoDescripcion:{
        fontSize:14,
        color:"#f7edff",
        margin:8,
        textAlign:"justify"
    },
    Iconos:{
        alignItems:"center",
        justifyContent:"center",
        height:"50%",
        width:"50%"
    },
    Boton:{
        marginTop:5,
        marginBottom:40
    }


})

export default Info