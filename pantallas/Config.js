import React from "react";
import {ScrollView, View, TouchableOpacity, StyleSheet} from "react-native";
import { EditarContacto, EditarUsuaria } from "../componentes/botonesConfig";
const Separadorlargo=()=>{
    return(<View style={estilos.Separadorlargo}/>)
}
const Config=()=>{
    return(
        <ScrollView style={estilos.Contenedor}>
            <Separadorlargo/>
        <View>
            <EditarUsuaria/>
        </View>
        <View>
            <EditarContacto/>
        </View>


    </ScrollView>

    )
}

const estilos=StyleSheet.create({
    Contenedor:{
        margin:0,
        padding:0,
        backgroundColor:'#B489F5'
    },
    Separadorlargo:{
        marginTop:100
    }

})

export default Config
