import React from "react";
import { Text, StyleSheet,ScrollView,View, FlatList } from "react-native";
import { MensajeContacto,UbicacionActual,PeligroInminente } from "../componentes/botones";



const Index=()=>{
    return(
        <ScrollView style={estilos.fondo}>
           
            <View style={estilos.contenedor}>
                <MensajeContacto/>
            </View>

            <View style={estilos.contenedor}>
                <UbicacionActual/>
            </View>

            <View style={estilos.contenedor}>
                <PeligroInminente/>
            </View>
                
                
            
            
        </ScrollView>
        

    )

}

const estilos=StyleSheet.create({
    
    fondo:{
        margin:0,
        padding:0,
        height:"100%",
        backgroundColor:'#4c2882'
    },
    
    
    
    contenedor:{
        marginTop:15,
        paddingTop:10,
        height:"30%",
        


    },
    ContenedorEntradaCorta:{
        marginTop:0,
        marginBottom:10,
        
        flexDirection:"row",
        justifyContent: "space-around"
    },
    ContenedorBoton:{
        flexBasis:"42%",
        padding:0,
        borderRadius:15,
        marginTop:10,
        marginBottom:10,
        marginLeft:15,
        marginRight:15,
        height:35,
        textAlign:"center"

    }
})

export default Index