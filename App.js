import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { Button, StyleSheet, Text, View  } from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import { createStackNavigator, useNavigation  } from "@react-navigation/stack";

const Stack= createStackNavigator();



import CrearUsuario from './pantallas/CrearUsuario';
import AnadirContacto from './pantallas/AnadirContacto';
import Index from './pantallas/Index';
import Registro from './pantallas/Registro';
import Login from './pantallas/Login';
import Config from './pantallas/Config';
import Info from './pantallas/Info';
import Localizacion from './pantallas/Localizacion';


function Stacks(){
  
  return(
    
    <Stack.Navigator>
      <Stack.Screen name="Index" component={Index}/>
      <Stack.Screen name="CrearUsuaria" component={CrearUsuario}/>
      <Stack.Screen name="AnadirContacto" component={AnadirContacto}/>
      <Stack.Screen name="Localizacion" component={Localizacion}/>
      <Stack.Screen name="Info" component={Info}/>
      <Stack.Screen name="Configuracion" component={Config}/>      
      <Stack.Screen name="Login" component={Login}/>
      <Stack.Screen name="Registro" component={Registro}/>
      
      
      
      
    </Stack.Navigator>
  )

}

export default function App() {
  return (
    <NavigationContainer>
      <Stacks/>
    </NavigationContainer>
  );
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#B489F5',
    alignItems: 'center',
    justifyContent: 'center',
  },
 
});
