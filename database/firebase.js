import firebase from "firebase";
import "firebase/firestore";

const firebaseConfig = {
    apiKey: "AIzaSyAUc6V1s-N76kdML22_cd_FjdhyhLr8G2Y",
    authDomain: "app-prev-vif.firebaseapp.com",
    projectId: "app-prev-vif",
    storageBucket: "app-prev-vif.appspot.com",
    messagingSenderId: "744240731528",
    appId: "1:744240731528:web:ebed5bd9a55b4e5bd4163d"
  };

  // Initialize Firebase
firebase.initializeApp(firebaseConfig);

const db=firebase.firestore();

export default {
    firebase,
    db

}