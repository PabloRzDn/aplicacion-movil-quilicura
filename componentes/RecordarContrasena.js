import React from "react";
import {TouchableOpacity,Text,View, StyleSheet} from "react-native";

export function RecordarContrasena(){
    return(
        
        <TouchableOpacity>
            <Text style={estilos.TextoClick} >¿Olvidaste tu contraseña?¡Click aquí!</Text>
        </TouchableOpacity>

    )

}

const estilos=StyleSheet.create({
    TextoClick:{
        textAlign:"center",
        fontSize:13,
        textDecorationLine:"underline"
    }
})
