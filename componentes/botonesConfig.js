import React from "react";
import { View, ScrollView, Text, TouchableOpacity, StyleSheet } from "react-native";


export function EditarUsuaria(){
    return(
        <TouchableOpacity style={{...estilos.Boton, backgroundColor:'#4c2882'}}>
            <View>
                <Text style={estilos.TextoBoton}> EDITAR USUARIA </Text>
            </View>


        </TouchableOpacity>

    )

} 

export function EditarContacto(){
    return(
        <TouchableOpacity style={{...estilos.Boton, backgroundColor:'#4c2882'}}>
            <View>
                <Text style={estilos.TextoBoton}> EDITAR CONTACTO </Text>
            </View>


            
        </TouchableOpacity>

    )

}


const estilos=StyleSheet.create({
    Boton:{
        marginTop:25,
        marginBottom:5,
        marginLeft:10,
        marginRight:10,
        borderRadius:10,
        height:70,
        alignContent:"center",
        
        
    },

    TextoBoton:{
        color:"#FFFFFF",
        textAlign:"center",
        margin:22.5,
        display:"flex",
        fontSize:15
    }


})