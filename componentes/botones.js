import React, { useEffect } from "react";
import {NavigationContainer, useNavigation} from '@react-navigation/native';
import {View,Text, StyleSheet, TouchableOpacity, Image} from "react-native";
import { createStackNavigator  } from "@react-navigation/stack";


//const navigation=useNavigation();

export function MensajeContacto(){
     
    const navigation=useNavigation();
    return(
        <TouchableOpacity
        onPress={()=>navigation.navigate("Info")} 
        style={{...estilos.boton, backgroundColor:"#31a86d"}}>
            <Image style={estilos.Imagenes} source={require("../statics/cookies-white.png")}/>
            <Text style={estilos.TextoBotones}> Borrar {"\n"} Cookies </Text>
            
        </TouchableOpacity>
    )

}

export function UbicacionActual(){

    const navigation=useNavigation();
            
    return(
        <TouchableOpacity 
        onPress={()=>navigation.navigate("Localizacion")}
        style={{...estilos.boton, backgroundColor:"#a2a831"}}>
            <Image style={estilos.Imagenes} source={require("../statics/ram-white.png")}/>
            <Text style={estilos.TextoBotones}> Optimizar {"\n"} RAM </Text>
            

        </TouchableOpacity>
    )

}



export function PeligroInminente(){
    return(
        <TouchableOpacity style={{...estilos.boton, 
        backgroundColor:"#A83131"}}>
            <Image style={estilos.Imagenes} source={require("../statics/memory-card-white.png")}/>
            <Text style={estilos.TextoBotones}> Limpiar {"\n"}Sistema </Text>

        </TouchableOpacity>
    )

}

const estilos=StyleSheet.create({
    boton:{
        flex:1,
        flexDirection:"row",
        marginTop:10,
        marginBottom:0,
        padding:10,
        width:"90%",
        height:"100%",
        alignSelf:"center",
        borderRadius:5,
        

    },
    TextoBotones:{
        flex:3,
        textAlign:"right",
        color:"#EDFFEF",
        fontWeight:"bold",
        fontSize:20,
        margin:20
        
    },
    Imagenes:{
        width:80,
        height:80,
        margin:10,
        alignSelf:"stretch"

    }
    

})

